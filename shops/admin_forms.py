from django import forms


class ShopImageForm(forms.ModelForm):
    class Meta:
        widgets = {
            'caption': forms.TextInput()
        }
