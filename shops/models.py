from django.db import models
from django.urls import reverse
from slugger import AutoSlugField
from progressiveimagefield.fields import ProgressiveImageField
from phonenumber_field.modelfields import PhoneNumberField
from malls.models import Mall
from shoppingmall.constants import SHOP_IMAGE_PATH


class Shop(models.Model):
    mall = models.ForeignKey(Mall, null=True, on_delete=models.SET_NULL, related_name='shops')
    name = models.CharField(max_length=200)
    shop_number = models.CharField(max_length=10)
    slug = AutoSlugField(populate_from='name', unique=True)
    summary = models.TextField(blank=True, default='')
    address = models.TextField()
    email = models.EmailField(null=True, blank=True)
    enquiry = PhoneNumberField(blank=False)
    open_time = models.TimeField(blank=True, null=True,)
    closing_time = models.TimeField(blank=True, null=True,)
    weekends = models.CharField(max_length=255, blank=True,)
    note = models.TextField(blank=True, default='')
    cover_photo = ProgressiveImageField(upload_to=SHOP_IMAGE_PATH, default='')
    cover_photo_caption = models.TextField(blank=True, default='')
    visiting_card = ProgressiveImageField(upload_to=SHOP_IMAGE_PATH, default='')

    def get_absolute_url(self):
        return reverse('shops:show', args=(self.slug,))

    def __str__(self):
        return self.name


class ShopImage(models.Model):
    shop = models.ForeignKey(Shop, null=True, on_delete=models.CASCADE, related_name='images')
    name = models.CharField(max_length=200)
    image = ProgressiveImageField(upload_to=SHOP_IMAGE_PATH, default='')
    caption = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name
