from django.contrib import admin
from .models import Shop
from .models import ShopImage
from .admin_forms import ShopImageForm


class ShopImageInline(admin.TabularInline):
    model = ShopImage
    form = ShopImageForm
    ordering = ('id',)
    extra = 0


class ShopAdmin(admin.ModelAdmin):
    inlines = [ShopImageInline, ]
    list_filter = ('weekends',)
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'shop_number', 'slug', 'enquiry', 'email', 'address', 'open_time', 'closing_time', 'weekends')
    search_fields = ('name', 'shop_number', 'slug', 'enquiry', 'email', 'address', 'open_time', 'closing_time', 'weekends')
    ordering = ('name',)
    list_display_links = ('name', 'shop_number', 'slug')
    list_per_page = 50


admin.site.register(Shop, ShopAdmin)
