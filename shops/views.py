from django.shortcuts import render
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from .models import Shop

# Create your views here.


def index(request):
    shops = Shop.objects.all().order_by('id')
    paginator = Paginator(shops, 50)
    per_page = request.GET.get('per_page')
    if per_page:
        paginator = Paginator(shops, per_page)

    shops = paginator.get_page(1)
    page = request.GET.get('page')
    if page:
        shops = paginator.get_page(page)

    context = {'objects': shops}
    return render(request, 'shops/index.html', context)


def show(request, slug):
    shop = get_object_or_404(Shop, slug=slug)
    context = {'shop':shop}

    return render(request, 'shops/show.html', context)
