"""Generate 30k fake shop images"""

from os import listdir
import random
from django.core.management.base import BaseCommand
from django.conf import settings
from faker import Faker
from shops.models import Shop
from shops.models import ShopImage
from shoppingmall.services import ImageThumb
from shoppingmall.constants import SHOP_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate 30k fake shop images'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        shop_image_files = listdir(settings.MEDIA_ROOT + SHOP_IMAGE_PATH)
        shops = Shop.objects.all()

        for index in range((10**3)*3):
            shop = random.choice(shops)
            name = fake.name()
            image_name = random.choice(shop_image_files)
            image = '{}{}'.format(SHOP_IMAGE_PATH, image_name)
            image_thumb_obj = ImageThumb()
            image_thumb_obj.generate_thumb(image_name, settings.MEDIA_ROOT + image, settings.MEDIA_ROOT + SHOP_IMAGE_PATH)
            caption = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)

            try:
                new_shop_image = ShopImage.objects.create(
                    shop=shop,
                    name=name,
                    image=image,
                    caption=caption
                )
            except:
                pass