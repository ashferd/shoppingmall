""""Generate shop images"""

import random
from django.core.management.base import BaseCommand
from django.conf import settings
from google_images_download import google_images_download
from shoppingmall.constants import SHOP_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate shop images'

    def handle(self, *args, **options):
        response = google_images_download.googleimagesdownload()
        items = ["shop", "shops"]
        # exact_sizes = ['723,747', '460,571']
        exact_sizes = ['250,250']

        for item in items:
            arguments = {
                "keywords": "{}".format(item),
                "format": "jpg",
                "exact_size": random.choice(exact_sizes),
                "type": 'photo',
                "limit": 100,
                "output_directory": settings.MEDIA_ROOT + SHOP_IMAGE_PATH,
                "no_directory": True,
                "no_numbering": True,
                "print_urls": True
            }

            paths = response.download(arguments)
            print(paths)
