"""Generate footer menu"""

from django.core.management.base import BaseCommand
from menu.models import Menu, MenuItem


class Command(BaseCommand):
    help = 'Generate footer menu'

    def handle(self, *args, **options):
        footer_menu = Menu(name='Footer menu', slug='footermenu', base_url='', description='Footer menu for shopping '
                                                                                           'mall')
        footer_menu.save()

        faq_menu_item = MenuItem(menu=footer_menu, order=1, link_url='/pages/faq', title='FAQ')
        faq_menu_item.save()

        terms_and_conditions_menu_item = MenuItem(menu=footer_menu, order=2, link_url='/pages/termsandconditions',
                                                  title='TERMS & CONDITIONS')
        terms_and_conditions_menu_item.save()

        privacy_policy_menu_item = MenuItem(menu=footer_menu, order=3, link_url='/pages/privacypolicy', title='PRIVACY '
                                                                                                              'POLICY')
        privacy_policy_menu_item.save()

        contact_menu_item = MenuItem(menu=footer_menu, order=4, link_url='/pages/contact', title='CONTACT')
        contact_menu_item.save()
