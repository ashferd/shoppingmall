"""Generate main menu"""

from django.core.management.base import BaseCommand
from menu.models import Menu, MenuItem


class Command(BaseCommand):
    help = 'Generate main menu'

    def handle(self, *args, **options):
        main_menu = Menu(name='Main menu', slug='mainmenu', base_url='', description='Main menu for shopping mall')
        main_menu.save()

        home_menu_item = MenuItem(menu=main_menu, order=1, link_url='/', title='HOME')
        home_menu_item.save()
        products_menu_item = MenuItem(menu=main_menu, order=2, link_url='/products', title='PRODUCTS')
        products_menu_item.save()
        shops_menu_item = MenuItem(menu=main_menu, order=3, link_url='/shops', title='SHOPS')
        shops_menu_item.save()
        malls_menu_item = MenuItem(menu=main_menu, order=4, link_url='/malls', title='MALLS')
        malls_menu_item.save()
