from django.apps import AppConfig


class ThirdPartyAppsDataInitiatorConfig(AppConfig):
    name = 'third_party_apps_data_initiator'
