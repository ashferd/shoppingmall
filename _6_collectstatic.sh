#!/usr/bin/env bash

echo "===================="
echo "===================="
echo "Collect static files"
echo "===================="
echo "===================="
# Generating css files from scss files.
scss static_common/scss/style.scss static_common/css/style.css

# Django command will collect all static files and gathered into static directory. If static directory is not
# available, it will be created automatically.
python manage.py collectstatic


# Copy static_common directory to static directory. To be more descriptive static_common directory contains
# sharable static files.
cp -r ./static_common ./static

echo "Static files collection is done."
echo "================================"
echo "================================"