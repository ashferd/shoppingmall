======================================================================================================
======================================================================================================
Setup self-signed security certificate with Openssl in Ubuntu 18.10 for Nginx server and google chrome
======================================================================================================
======================================================================================================
To get self-signed security certificate in local server(usually we called it localhost or
127.0.0.1) we should follow these steps:

## Install OpenSSL:
===================
sudo apt install openssl

## Create a directory "ssl_certificate" where you will generate your files.
===========================================================================
mkdir ssl_certificate

## Enter that directory and create a conf file, in my case, local.shoppingmall.com.conf.
========================================================================================
cd ssl_certificate; gedit local.shoppingmall.com.conf

## Open local.shoppingmall.com.conf and write this:
===================================================
----------------------------------------------------------------------------
[req]
default_bits       = 2048
default_keyfile    = local.shoppingmall.com.key
distinguished_name = req_distinguished_name
req_extensions     = req_ext
x509_extensions    = v3_ca

[req_distinguished_name]
countryName                 = Country Name (2 letter code)
countryName_default         = US
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = New York
localityName                = Locality Name (eg, city)
localityName_default        = Rochester
organizationName            = Organization Name (eg, company)
organizationName_default    = local.shoppingmall.com
organizationalUnitName      = organizationalunit
organizationalUnitName_default = Development
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_default          = local.shoppingmall.com
commonName_max              = 64

[req_ext]
subjectAltName = @alt_names

[v3_ca]
subjectAltName = @alt_names

[alt_names]
DNS.1   = local.shoppingmall.com
DNS.2   = 127.0.0.1
----------------------------------------------------------------------------
Note, I use local.shoppingmall.com in multiple places. Actually, I already have an application
running in nginx server where I create vhost for that application and that vhost file name is
local.shoppingmall.com inside /etc/nginx/sites-available. I also configure local.shoppingmall.com
as domain name inside that vhost file and also in /etc/hosts file. To make that process synchronized,
I am using local.shoppingmall.com in everywhere. You can use your domain name to make the process
synchronized.

## Create the Certificate using OpenSSL:
========================================
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout local.shoppingmall.com.key -out local.shoppingmall.com.crt -config local.shoppingmall.com.conf

## Copy the public key to the /etc/ssl/certs directory
======================================================
sudo cp localhost.crt /etc/ssl/certs/local.shoppingmall.com.crt

## Copy the private key to the /etc/ssl/private directory
=========================================================
sudo cp localhost.key /etc/ssl/private/local.shoppingmall.com.key

## Update the Nginx Configuration File to Load the Certificate Key Pair
=======================================================================
sudo nano /etc/nginx/sites-available/local.shoppingmall.com.conf

## Replace the content with this:
=================================
-----------------------------------------------------------------------
server {
    listen 80;
    listen [::]:80;

    server_name local.shoppingmall.com;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name local.shoppingmall.com;

    ssl_certificate /etc/ssl/certs/local.shoppingmall.com.crt;
    ssl_certificate_key /etc/ssl/private/local.shoppingmall.com.key;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/roman/hdd/1_works/python/django_projects/shoppingmall;
    }

    location /media/ {
        root /home/roman/hdd/1_works/python/django_projects/shoppingmall;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
-----------------------------------------------------------------------
This content will redirect http request to https. Also certification file locations are declared
here.

## Reload the Nginx configuration changes:
==========================================
sudo service nginx reload

## Although everything is done google chrome and anyother browser will show insecure message.
=============================================================================================
To fix this:
============

## Add the certificate to the trusted CA root store:
====================================================
certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n "local.shoppingmall.com" -i local.shoppingmall.com.crt

## Close google chrome.
=======================
## Restart and it should be done now. The https should work correctly as it is.
===============================================================================

================================================================================================
================================================================================================
Setup django-haystack, solr-6.5 in Ubuntu 18.10 for Django 2.1 application
================================================================================================
================================================================================================
Install pysolr:
===============
## Go to django project root directory:
=======================================
cd ~/hdd/1_works/python/django_projects/shoppingmall

## Install pysolr by pip inside virtualenv:
===========================================
pip install pysolr

Note, pysolr package is a dependency to work with solr server, when you are using django-haystack.

Install django-haystack:
========================
## Go to django project root directory:
=======================================
cd ~/hdd/1_works/python/django_projects/shoppingmall

## Install django-haystack by pip inside virtualenv:
====================================================
pip install django-haystack

Add, 'haystack', in the INSTALLED_APPS list.

Install solr-6.5:
=================
## Go to apache-solr download url to download solr-6.6.5:
=========================================================
https://www-us.apache.org/dist/lucene/solr/6.6.5/

## Copy link address of solr-6.6.5.tgz file:
============================================
https://www-us.apache.org/dist/lucene/solr/6.6.5/solr-6.6.5.tgz

## Download it:
===============
wget https://www-us.apache.org/dist/lucene/solr/6.6.5/solr-6.6.5.tgz

## Copy the file to /opt directory
==================================
sudo cp solr-6.6.5.tgz /opt/

## Unpack and install:
======================
tar xzf solr-6.6.5.tgz solr-7.5.0/bin/install_solr_service.sh --strip-components=2
sudo bash ./install_solr_service.sh solr-6.6.5.tgz

Configure solr-6.5:
==================
## Create a new core:
=====================
sudo -u solr bin/solr create_core -c shoppingmall

You can now configure django-haystack to communicate with solr to manage and use indexing.

However there are some commands you may need to know. For example:
-----------------------------------------------------------------
Main Directory:
===============
cd /opt/solr

Core directory:
===============
sudo su -;
cd /var/solr/data/[Your Schema]

Conf directory for core:
========================
cd /var/solr/data/[Your Schema]/conf

Most important configuration files for core:
============================================
solrconfig.xml
managed-schema
schema.xml

Directory permission:
====================
chown -R solr:solr /var/solr
chmod u+w -R /var/solr

Solr server commands:
=====================
sudo systemctl start solr.service
sudo systemctl status solr.service
sudo systemctl restart solr.service
sudo systemctl stop solr.service

Solr default port:
==================
8983

Solr admin url:
===============
localhost:8983 or
127.0.0.1:8983

Solr create core command:
=========================
sudo -u solr bin/solr create_core -c flims

copy "conf" directory from "opt/solr-7.7.1/server/solr/configsets/_default" to "/var/solr/data/films":
=====================================================================================================
sudo cp -r opt/solr-7.7.1/server/solr/configsets/_default /var/solr/data/films

Create sore field from ui as name and text_general type and create a copy field from command line:
==================================================================================================
curl -X POST -H "Content-type: application/json" --data-binary '{"add-copy-field" : {"source":"*", "dest":"_text_"}}' http://127.0.0.1:8983/solr/films/schema

Use post tool to populate films schema from example directory:
==============================================================
sudo -u solr bin/post -c films example/films/films.xml

Solr delete command for data delete:
====================================
curl http://127.0.0.1:8983/solr/films/update?commit=true -H "Content-type: text/xml" --data-binary '<delete><query>*:*</query></delete>'

Solr delete core command:
=========================
sudo -u solr bin/solr delete -c flims

Solr uninstallation:
====================
sudo service solr stop
sudo rm -r /var/solr
sudo rm -r /opt/solr-5.3.1
sudo rm -r /opt/solr
sudo rm /etc/init.d/solr
sudo deluser --remove-home solr
sudo deluser --group solr
sudo rm -rf /etc/default/solr.in.sh
-----------------------------------------------------------------

Configure django-settings for django-haystack:
==============================================
## After configuring solr-6.6.5 anc creating core, open shoppingmall/settings.py and add:
=========================================================================================
--------------------------------------------------------------------
# django-haystack configurations.
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://localhost:8983/solr/shoppingmall',
        'ADMIN_URL': 'http://localhost:8983/solr/admin/cores'
        # ...or for multicore...
        # 'URL': 'http://127.0.0.1:8983/solr/mysite',
    },
}
--------------------------------------------------------------------
Now, django-haystack can communicate with solr when the service is active.

django-haystack commands:
=========================
From django project rood directory, type "python manage.py help". You will get available django-haystack
commands in "[haystack]" section.

Update core config files for solr-6.6.5 with django-haystack commands to make indexing and searching workable:
==============================================================================================================
Follow these instructions:
--------------------------------------------------------------------------------------------------
==================================================================================================
==================================================================================================
INSTRUCTIONS TO FIX "solrconfig.xml" FILE TO MAKE WORKABLE schema.xml FILE WITHOUT "managed-schema"
==================================================================================================
==================================================================================================
1] Open "solrconfig.xml" file from "/var/solr/data/[Your Schema]/conf" directory.

2] Search for "<directoryFactory name="DirectoryFactory" class="${solr.directoryFactory:solr.NRTCachingDirectoryFactory}"/>" and "<codecFactory class="solr.SchemaCodecFactory"/>" in "solrconfig.xml" file.

3] Bellow "<directoryFactory name="DirectoryFactory" class="${solr.directoryFactory:solr.NRTCachingDirectoryFactory}"/>" and above "<codecFactory class="solr.SchemaCodecFactory"/>", add "<schemaFactory class="ClassicIndexSchemaFactory"/>"

4]
    a) For solr 6.5: Search for the following XML element and comment the whole element: "<processor class="solr.AddSchemaFieldsUpdateProcessorFactory">"
    b) Search "${update.autoCreateFields:true}" and replace it with "${update.autoCreateFields:false}".

5] Rename "managed-schema" to "schema.xml" which is in "/var/solr/data/[Your Schema]/conf" directory.

6] Open "schema.xml" file. Search and remove "<field name="id" type="string" indexed="true" stored="true" required="true" multiValued="false" />"

7] Create a directory to your project root: "docs/solr/schema/"

8] Copy "schema.xml" from "/var/solr/data/[Your Schema]/conf" directory to "docs/solr/schema/" directory. We need to change it in near future.

9] Generate haystack based "schema.xml" file by running the command: "python manage.py build_solr_schema > docs/solr/schema/haystack-generated-schema.xml"

10] Open "docs/solr/schema/haystack-generated-schema.xml" file.

11] Find:
<!--
    ######################## django-haystack specifics begin ########################
    -->
and
<!--
    ######################## django-haystack specifics end ########################
    -->

12] Copy all the contents between these two comments.

13] Open "docs/solr/schema/schema.xml".

14] Paste the copied content just after starting "schema" tag.

15] Replace "/var/solr/data/[Your Schema]/conf/schema.xml" with "docs/solr/schema/schema.xml".

16] Restart solr: "sudo systemctl restart solr.service"

[TESTED ONLY solr-6.5 and works perfectly! Although, solr-7.7.1 don't complain in this process, search "somehow" does not work for it!]
==========================================================================================================
==========================================================================================================
END OF INSTRUCTIONS TO FIX "solrconfig.xml" FILE TO MAKE WORKABLE schema.xml FILE WITHOUT "managed-schema"
==========================================================================================================
==========================================================================================================
----------------------------------------------------------------------------------------------------------

Create search indexes:
======================
## Read this document and do as instructed in django project:
=============================================================
https://django-haystack.readthedocs.io/en/master/tutorial.html

## If everything is done, run haystack command:
===============================================
python manage.py rebuild_index

This should rebuild index in solr.

To update index for further changes in data in indexable fields, run this command:
python manage.py update_index

## The search should work now.
==============================
You may check the search functionality from solr admin as well as your django application.

================================================================================================
================================================================================================
Setup celery, rabbitmq, celery-beat to run job queue in Ubuntu 18.10 for Django 2.1 application
================================================================================================
================================================================================================
Install celery:
===============
## Install Celery by pip inside virtualenv:
===========================================
pip install Celery

Install rabbitmq:
=================
## To install rabbitmq, type:
=============================
sudo apt update
sudo apt -y install rabbitmq-server

Configure rabbitmq:
===================
## To use Celery we need to create a RabbitMQ user, a virtual host and allow that user access to that virtual host:
===================================================================================================================
$ sudo rabbitmqctl add_user myuser mypassword
$ sudo rabbitmqctl add_vhost myvhost
$ sudo rabbitmqctl set_user_tags myuser mytag
$ sudo rabbitmqctl set_permissions -p myvhost myuser ".*" ".*" ".*"

Substitute in appropriate values for myuser, mypassword and myvhost above.

See the RabbitMQ Admin Guide for more information about access control.

## Application specific example:
================================
# https://stackoverflow.com/questions/14699873/how-to-reset-user-for-rabbitmq-management
sudo rabbitmqctl add_user shoppingmall shoppingmall
sudo rabbitmqctl add_vhost /
sudo rabbitmqctl set_user_tags shoppingmall administrator
sudo rabbitmqctl set_permissions -p / shoppingmall ".*" ".*" ".*"

## Additional information:
==========================
---------------------------------------------------------------------------------------------------
Some useful commands:
======================
sudo rabbitmqctl clear_permissions -p / shoppingmall
sudo rabbitmqctl delete_vhost /

Starting/Stopping the RabbitMQ server
To start the server:

$ sudo rabbitmq-server
you can also run it in the background by adding the -detached option (note: only one dash):

$ sudo rabbitmq-server -detached

Check rabbitmq node:
====================
sudo rabbitmqctl status

Stop rabbitmq server:
=====================
sudo rabbitmqctl stop

Get all rabbitmq commands:
==========================
sudo rabbitmqctl

Rabbitmq management plugin
==========================
# https://www.rabbitmq.com/management.html
sudo rabbitmq-plugins enable rabbitmq_management

http://127.0.1.1:15672/
user/pass:shoppingmall/shoppingmall
---------------------------------------------------------------------------------------------------

Configure django-settings for celery:
=====================================
## Open shoppingmall/settings.py and add:
=========================================
---------------------------------------------------------------------------------------
# Celery confiburations.
CELERY_BROKER_URL = 'amqp://shoppingmall:shoppingmall@roman-HP-ProBook-450-G2:5672//'
CELERY_TIMEZONE = 'Asia/Dhaka' # https://gist.github.com/pamelafox/986163

# Using celery_beat instead of django_celery_beat.
CELERY_BEAT_SCHEDULE = {
    'update_solr_index': {
        'task': 'update_solr_index',
        'schedule': 120.0,
    }
}
---------------------------------------------------------------------------------------

Install django-celery-beat:
===========================
## Install django-celery-beat by pip inside virtualenv:
=======================================================

############################################
django_celery_beat does not work atm. It
has some issue with celery heartbeat to
start and sending task to amqp.

I am using celery_beat instead.
############################################
pip install django-celery-beat==1.1.0

Note, latest django-celery-beat has serious migration issue. Till that fix, we need to use django-celery-beat==1.1.0

Add, 'django_celery_beat', in the INSTALLED_APPS list.

Configure django-celery-beat:
=============================
## Run migration:
=================
python manage.py migrate

Configure Celery to work with django application:
====================================================
## Create celery.py file inside shoppingmall/shoppingmall directory:
====================================================================
touch celery.py

## Open celery.py file and add this code snippet:
================================================
-----------------------------------------------------------------------
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'shoppingmall.settings')

app = Celery('shoppingmall')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


# @app.task(bind=True)
# def debug_task(self):
#     print('Request: {0!r}'.format(self.request))

# @app.task(bind=True)
# def update_solr_index(self):
#     from haystack.management.commands import update_index
#     update_index.Command().handle(using=['default'], remove=True)
-----------------------------------------------------------------------

## Open __init__.py file of shoppingmall/shoppingmall and add this code snippet:
================================================================================
--------------------------------------------------------------------------------
from __future__ import absolute_import, unicode_literals

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app

__all__ = ('celery_app',)
--------------------------------------------------------------------------------

Write a Celery task:
====================
## Create tasks.py inside front app:
====================================
touch front/tasks.py

## Open front/tasks.py and add this code snippt:
================================================
-------------------------------------------------------------------
from celery import task

@task(name='update_solr_index')
def update_solr_index():
    from haystack.management.commands import update_index
    update_index.Command().handle(using=['default'], remove=True)
-------------------------------------------------------------------

Assign Celery task from django-celery-beat admin panel:
==========================================================
## Login to django admin panel and check django-celery-beat section.
====================================================================
## Insert a new interval.
=========================
## Add a new periodic task with newly created interval and select the task name just written at tasks.py file.
==============================================================================================================
## Save it.
===========

Run celery for development:
===========================
## Run:
=======
celery -A shoppingmall  worker -l info -B --scheduler django_celery_beat.schedulers.DatabaseScheduler

This is enough to run task for development environment. However, daemonize may help to work in production env.

Run celery with supervisor for production:
==========================================
## From reference url:
https://realpython.com/asynchronous-tasks-with-django-and-celery/
http://docs.celeryproject.org/en/latest/userguide/daemonizing.html#daemonizing
https://github.com/celery/celery/issues/4184
========================================================================================

Install supervisor:
===================
sudo apt-get install supervisor -y

We then need to tell Supervisor about our Celery workers by adding configuration files to the
“/etc/supervisor/conf.d/” directory on the remote server. In our case, we need two such configuration
files - one for the Celery worker and one for the Celery scheduler.

Locally, create a folder called "docs/supervisor" in the project root. Then add the following files…
----------------------------------------------------------------------------------------------------
; shoppingmall_celery.conf file

; ==================================
;  celery worker supervisor example
; ==================================

; the name of your supervisord program
[program:shoppingmallcelery]

; Set full path to celery program if using virtualenv
command=/home/roman/hdd/1_works/python/django_projects/shoppingmall/env_shoppingmall/bin/celery -A shoppingmall worker --loglevel=WARNING

; The directory to your Django project
directory=/home/roman/hdd/1_works/python/django_projects/shoppingmall

; If supervisord is run as the root user, switch users to this UNIX user account
; before doing any processing.
user=roman

; Supervisor will start as many instances of this program as named by numprocs
numprocs=1

; Put process stdout output in this file
stdout_logfile=/var/log/celery/shoppingmall_worker.log

; Put process stderr output in this file
stderr_logfile=/var/log/celery/shoppingmall_worker.log

; If true, this program will start automatically when supervisord is started
autostart=true

; May be one of false, unexpected, or true. If false, the process will never
; be autorestarted. If unexpected, the process will be restart when the program
; exits with an exit code that is not one of the exit codes associated with this
; process’ configuration (see exitcodes). If true, the process will be
; unconditionally restarted when it exits, without regard to its exit code.
autorestart=true

; The total number of seconds which the program needs to stay running after
; a startup to consider the start successful.
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

; When resorting to send SIGKILL to the program to terminate it
; send SIGKILL to its whole process group instead,
; taking care of its children as well.
killasgroup=true

; if your broker is supervised, set its priority higher
; so it starts first
priority=998
----------------------------------------------------------------------------------------------------

and

----------------------------------------------------------------------------------------------------
; shoppingmall_celerybeat.conf file

; ================================
;  celery beat supervisor example
; ================================

; the name of your supervisord program
[program:shoppingmallcelerybeat]

; Set full path to celery program if using virtualenv
command=/home/roman/hdd/1_works/python/django_projects/shoppingmall/env_shoppingmall/bin/celery -A shoppingmall beat --loglevel=WARNING --scheduler django_celery_beat.schedulers:DatabaseScheduler

; The directory to your Django project
directory=/home/roman/hdd/1_works/python/django_projects/shoppingmall

; If supervisord is run as the root user, switch users to this UNIX user account
; before doing any processing.
user=roman

; Supervisor will start as many instances of this program as named by numprocs
numprocs=1

; Put process stdout output in this file
stdout_logfile=/var/log/celery/shoppingmall_beat.log

; Put process stderr output in this file
stderr_logfile=/var/log/celery/shoppingmall_beat.log

; If true, this program will start automatically when supervisord is started
autostart=true

; May be one of false, unexpected, or true. If false, the process will never
; be autorestarted. If unexpected, the process will be restart when the program
; exits with an exit code that is not one of the exit codes associated with this
; process’ configuration (see exitcodes). If true, the process will be
; unconditionally restarted when it exits, without regard to its exit code.
autorestart=true

; The total number of seconds which the program needs to stay running after
; a startup to consider the start successful.
startsecs=10

; if your broker is supervised, set its priority higher
; so it starts first
priority=999
----------------------------------------------------------------------------------------------------

Make sure to update the paths in these files to match the remote server’s filesystem.

Basically, these supervisor configuration files tell supervisord how to run and manage our 'programs' (as they are called by supervisord).

In the examples above, we have created two supervisord programs named "shoppingmallcelery" and "shoppingmallcelerybeat".

Now just copy these files to the remote server in the “/etc/supervisor/conf.d/” directory.

We also need to create the log files that are mentioned in the above scripts on the remote server:

$ touch /var/log/celery/shoppingmall_worker.log
$ touch /var/log/celery/shoppingmall_beat.log

Finally, run the following commands to make Supervisor aware of the programs - e.g., shoppingmallcelery and shoppingmallcelerybeat:

$ sudo supervisorctl reread
$ sudo supervisorctl update

Run the following commands to stop, start, and/or check the status of the shoppingmallcelery program:

$ sudo supervisorctl stop shoppingmallcelery
$ sudo supervisorctl start shoppingmallcelery
$ sudo supervisorctl status shoppingmallcelery

and

Run the following commands to stop, start, and/or check the status of the shoppingmallcelerybeat program:

$ sudo supervisorctl stop shoppingmallcelerybeat
$ sudo supervisorctl start shoppingmallcelerybeat
$ sudo supervisorctl status shoppingmallcelerybeat


Final Tips
Do not pass Django model objects to Celery tasks. To avoid cases where the model object has already changed before
it is passed to a Celery task, pass the object’s primary key to Celery. You would then, of course, have to use the
primary key to get the object from the database before working on it.

The default Celery scheduler creates some files to store its schedule locally. These files would be
"celerybeat-schedule.db" and "celerybeat.pid". If you are using a version control system like Git
(which you should!), it is a good idea to ignore this files and not add them to your repository since
they are for running processes locally.


######### IMPORTANT ##################
=======================================
"Somehow", "Sometimes" celery heartbeat does not work correctly then you may off the supervisor processes.
----------------------------------------------------
sudo supervisorctl stop shoppingmallcelery
sudo supervisorctl stop shoppingmallcelerybeat
sudo systemctl stop supervisor.service
----------------------------------------------------
and run this to debug:
----------------------------------------------------
celery -A shoppingmall worker -B
----------------------------------------------------
However, this should ONLY run on development and NOT in PRODUCTION. This is only suitable for debugging.