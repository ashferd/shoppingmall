#!/usr/bin/env bash

echo "Start faking data!"
echo "======================"
echo "======================"
echo "Collecting mall images from google image."
python manage.py generate_mall_images
echo "Collecting mall images is done!"
echo "======================"
echo "Renaming mall images"
python manage.py rename_malls_images
echo "Renaming mall images is done!"
echo "======================"
echo "Creating Malls!"
python manage.py generate_1k_fake_malls
echo "Mall creation is done!"
echo "======================"
echo "Creating mall images entry."
python manage.py generate_1k_fake_mall_images
echo "Mall images creation is done!"
echo "======================"
echo "Collecting shop images from google image."
python manage.py generate_shop_images
echo "Collecting shop images is done!"
echo "======================"
echo "Renaming shop images"
python manage.py rename_shops_images
echo "Renaming shop images is done!"
echo "======================"
echo "Creating shops!"
python manage.py generate_10k_fake_shops
echo "Shop creation is done!"
echo "======================"
echo "Creating shop images entry."
python manage.py generate_3k_fake_shop_images
echo "Shop images creation is done!"
echo "======================"
echo "Collecting product category images from google image."
python manage.py generate_product_category_images
echo "Collecting product category images is done!"
echo "======================"
echo "Renaming product category images"
python manage.py rename_product_categories_images
echo "Renaming product category images is done!"
echo "======================"
echo "Creating categories!"
python manage.py generate_10_fake_categories
echo "Category creation is done!"
echo "======================"
echo "Creating sub-categories!"
python manage.py generate_50_fake_subcategories
echo "Sub-category creation is done!"
echo "======================"
echo "Collecting product images from google image."
python manage.py generate_product_images
echo "Collecting product images is done!"
echo "======================"
echo "Renaming product images"
python manage.py rename_products_images
echo "Renaming product images is done!"
echo "======================"
echo "Creating products!"
python manage.py generate_100k_fake_products
echo "Product creation is done!"
echo "======================"
echo "======================"
echo "Creating product images entry."
python manage.py generate_30k_fake_product_images
echo "Product images creation is done!"
echo "======================"
echo "======================"
echo "Data is prepared! Done!!"