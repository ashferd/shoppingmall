server {
    listen 80;
    listen [::]:80

    server_name local.shoppingmall.com;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/roman/hdd/1_works/python/django_projects/shoppingmall;
    }

    location /media/ {
        root /home/roman/hdd/1_works/python/django_projects/shoppingmall;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
