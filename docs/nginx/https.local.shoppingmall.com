server {
    listen 80;
    listen [::]:80;
    
    server_name local.shoppingmall.com;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name local.shoppingmall.com;

    ssl_certificate /etc/ssl/certs/local.shoppingmall.com.crt;
    ssl_certificate_key /etc/ssl/private/local.shoppingmall.com.key;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/roman/hdd/1_works/python/django_projects/shoppingmall;
    }

    location /media/ {
        root /home/roman/hdd/1_works/python/django_projects/shoppingmall;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
