#!/usr/bin/env bash

echo "========================="
echo "========================="
echo "Reverting all migrations."
echo "========================="
echo "========================="
echo "Reverting admin."
python manage.py migrate admin zero
echo "Reverting admin is done."
echo "Reverting auth."
python manage.py migrate auth zero
echo "Reverting auth is done."
echo "Reverting contenttypes."
python manage.py migrate contenttypes zero
echo "Reverting contenttypes is done."
echo "Reverting sessions."
python manage.py migrate sessions zero
echo "Reverting sessions is done."
echo "Reverting menu."
python manage.py migrate menu zero
echo "Reverting menu is done."
echo "Reverting malls."
python manage.py migrate malls zero
echo "Reverting malls is done."
echo "Reverting shops."
python manage.py migrate shops zero
echo "Reverting shops is done."
echo "Reverting products."
python manage.py migrate products zero
echo "Reverting products is done."
echo "Show migrations status."
python manage.py showmigrations
echo "All migrations are reverted."
echo "============================"
echo "============================"