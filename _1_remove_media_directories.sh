#!/usr/bin/env bash

echo "==========================="
echo "==========================="
echo "Remove \"media\" directory."
echo "==========================="
echo "==========================="
echo "Entering \"media\" directory."
cd media/
echo "Removing all from \"media\" directory."
sudo rm -Rf *
echo "Removing all from \"media\" directory is done."
echo "=============================================="
echo "=============================================="
