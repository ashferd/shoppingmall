from haystack.generic_views import SearchView as HaystackSearchView, FacetedSearchView as HaystackFacetedSearchView
from .forms import FacetedSearchForm


class BasicSearchView(HaystackSearchView):
    form_name = 'basic_search_form'
    template_name = 'basic/search.html'


class FacetedSearchView(HaystackFacetedSearchView):
    form_class = FacetedSearchForm
    facet_fields = ['searchable_price', 'in_stock']
    form_name = 'faceted_search_form'
    template_name = 'faceted/search.html'
