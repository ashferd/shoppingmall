from .forms import BasicSearchForm
from django.urls import reverse


def basic_search_form_context_processor(request):
    return {
        'basic_search_form': BasicSearchForm(),
        'basic_search_form_url': reverse("search:basic", )
    }
