from django import forms
from haystack.forms import SearchForm as HaystackBasicSearchForm, FacetedSearchForm as HaystackFacetedSearchForm


class BasicSearchForm(HaystackBasicSearchForm):
    pass


class FacetedSearchForm(HaystackFacetedSearchForm):
    q = None

    searchable_price = forms.CharField(required=False, label=None, widget=forms.TextInput(attrs={
        'class': 'price-range-slider '
                 'hide-text-field',
        'type': 'text',
        'data-type': 'double',
        'data-min': '0',
        'data-max': '1000000',
        'data-from': '0',
        'data-to': '1000000',
        'data-grid': 'true'
    }))

    in_stock = forms.BooleanField(required=False, label='In Stock?', widget=forms.CheckboxInput({'checked': True}))

    def __init__(self, *args, **kwargs):
        super(FacetedSearchForm, self).__init__(*args, **kwargs)

    def search(self):
        searchable_price = self.cleaned_data['searchable_price']
        in_stock = self.cleaned_data['in_stock']

        sqs = self.searchqueryset

        if not self.is_valid():
            return self.no_query_found()

        if searchable_price:
            price_range_start, price_range_end = searchable_price.split(";")

            if price_range_start:
                sqs = sqs.filter(searchable_price__gte=price_range_start)

            if price_range_end:
                sqs = sqs.filter(searchable_price__lte=price_range_end)

        sqs = sqs.filter(in_stock=in_stock)

        return sqs
