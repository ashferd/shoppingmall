# encoding: utf-8

from django.urls import path
from .views import BasicSearchView, FacetedSearchView

app_name = 'search'
urlpatterns = [
    path('faceted', FacetedSearchView.as_view(), name='faceted'),
    path('', BasicSearchView.as_view(), name='basic'),
]
