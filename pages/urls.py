from django.urls import path
from . import views


app_name = 'pages'
urlpatterns = [
    path('faq', views.faq, name='faq'),
    path('termsandconditions', views.termsandconditions, name='termsandconditions'),
    path('privacypolicy', views.privacypolicy, name='privacypolicy'),
    path('contact', views.contact, name='contact'),
]
