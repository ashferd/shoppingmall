from django.shortcuts import render


def faq(request):
    return render(request, 'pages/faq.html')


def termsandconditions(request):
    return render(request, 'pages/termsandconditions.html')


def privacypolicy(request):
    return render(request, 'pages/privacypolicy.html')


def contact(request):
    return render(request, 'pages/contact.html')
