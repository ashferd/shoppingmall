#!/usr/bin/env bash

./_0_revert_migrations.sh;
./_1_remove_media_directories.sh;
./_2_remove_db_migration_files.sh;
./_3_migrate_db.sh;
./_4_create_super_user.sh;
./_5_rebuild_site.sh;
./_6_collectstatic.sh;