(function ($) {
    'use strict';

    $(document).ready(function () {
        // -----------------------------
        //  On Scroll Resize Nav
        // -----------------------------
        $(document).on('scroll', function() {
            if($(document).scrollTop() > 200) {
                $('.main-nav').removeClass('large');
                if (window.matchMedia('(min-width: 768px)').matches) {
                    $('.search-wrapper').css('top', '48px');
                }
            } else {
                $('.main-nav').addClass('large');
                if (window.matchMedia('(min-width: 768px)').matches) {
                    $('.search-wrapper').css('top', '74px');
                }
            }
        });
        // -----------------------------
        //  On Click Smooth scrool
        // -----------------------------
        /*$('.scrollTo').on('click', function(e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $('html, body').animate({
                scrollTop: ($(target).offset().top)
            }, 500);
        });*/
        // -----------------------------
        //  Testimonial Slider
        // -----------------------------
        /*$('.testimonial-slider').slick({
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });*/
        // -----------------------------
        //  Screenshot Slider
        // -----------------------------
        /*$('.screenshot-slider').slick({
            dots: true,
            slidesToShow: 3,
            centerMode: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });*/
        // -----------------------------
        //  Video Replace
        // -----------------------------
        /*$('.video-box span.icon').click(function() {
            var video = '<iframe allowfullscreen src="' + $(this).attr('data-video') + '"></iframe>';
            $(this).replaceWith(video);
        });*/
        // -----------------------------
        //  Team Progress Bar
        // -----------------------------
        /*$('.team').waypoint(function(){
            $('.progress').each(function(){
                $(this).find('.progress-bar').animate({
                    width:$(this).attr('data-percent')
                });
            });
            this.destroy();
        },{
            offset:100
        });*/

    });


})(jQuery);