from products.models import Category, Product


def get_storefront_products():
    """

    :return:
    """
    storefront_products = []

    for category in _get_categories():
        sub_categories = _get_sub_categories_by_category(category)
        products = _get_products_by_categories(sub_categories)

        storefront_product = StorefrontProduct(category, sub_categories, products)
        storefront_products.append(storefront_product)

    return storefront_products


def _get_categories():
    """

    :return:
    """
    return Category.objects.filter(parent__isnull=True)


def _get_sub_categories_by_category(category):
    """

    :param category:
    :return:
    """
    sub_categories = Category.objects.filter(parent=category)
    return sub_categories


def _get_products_by_categories(categories):
    """

    :param categories:
    :return:
    """
    products = Product.objects.filter(category__in=categories)[:10]
    return products


class StorefrontProduct(object):
    """

    """
    def __init__(self, category, sub_categories=None, products=None):
        self.category = category
        if sub_categories is None:
            self.sub_categories = []
        else:
            self.sub_categories = sub_categories

        if products is None:
            self.products = []
        else:
            self.products = products
