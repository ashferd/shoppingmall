from django.shortcuts import render
from .storefront_products import get_storefront_products
# Create your views here.


def index(request):
    context = {'storefront_products': get_storefront_products()}
    return render(request, 'front/index.html', context)
