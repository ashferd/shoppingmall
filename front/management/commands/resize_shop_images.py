import os
from fractions import Fraction
from PIL import Image
from django.conf import settings
from django.core.management.base import BaseCommand
from shoppingmall.constants import EXTRA_SHOP_IMAGE_PATH, SHOP_IMAGE_PATH


class Command(BaseCommand):
    # https://gist.github.com/zed/4221180
    help = 'Resize shop images'

    def handle(self, *args, **options):
        shop_image_files = os.listdir(EXTRA_SHOP_IMAGE_PATH)
        size = (250, 250, )

        os.makedirs((settings.MEDIA_ROOT + SHOP_IMAGE_PATH), exist_ok=True)

        for i in shop_image_files:
            self.crop_resize_mp(EXTRA_SHOP_IMAGE_PATH + i, size, Fraction(*size))

    def crop_resize_mp(self, input_filename, size, ratio):
        try:
            image = self.crop_resize(Image.open(input_filename), size, ratio)

            # save resized image
            basename, ext = os.path.splitext(os.path.basename(input_filename))
            output_basename = basename + ext
            output_filename = os.path.join(settings.MEDIA_ROOT + SHOP_IMAGE_PATH, output_basename)
            image.save(output_filename)

            return (input_filename, output_filename), None
        except Exception as e:
            return (input_filename, None), e

    def crop_resize(self, image, size, ratio):
        # crop to ratio, center
        w, h = image.size
        if w > ratio * h:  # width is larger then necessary
            x, y = (w - ratio * h) // 2, 0
        else:  # ratio*height >= width (height is larger)
            x, y = 0, (h - w / ratio) // 2
        image = image.crop((x, y, w - x, h - y))

        # resize
        if image.size > size:  # don't stretch smaller images
            image.thumbnail(size, Image.ANTIALIAS)
        return image
