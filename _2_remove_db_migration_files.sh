#!/usr/bin/env bash

echo "================================"
echo "================================"
echo "Remove database migration files."
echo "================================"
echo "================================"
echo "Removing migrations file from malls."
cd malls/migrations
sudo find . -type f ! -name '__init__.py' -delete

echo "Removing migrations file from shops."
cd ../../shops/migrations
sudo find . -type f ! -name '__init__.py' -delete

echo "Removing migrations file from products."
cd ../../products/migrations
sudo find . -type f ! -name '__init__.py' -delete
echo "Remove database migration files are done."
echo "========================================="
echo "========================================="