#!/usr/bin/env bash

echo "======================"
echo "======================"
echo "Generate migrations"
echo "======================"
echo "======================"
echo "Making new migration files."
python manage.py makemigrations
echo "Making new migration files are done."
echo "======================"
echo "Running migration."
python manage.py migrate
echo "Running migration is done."
echo "======================"
echo "Database schema migration is done."
echo "=================================="
echo "=================================="