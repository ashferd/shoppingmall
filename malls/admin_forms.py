from django import forms


class MallImageForm(forms.ModelForm):
    class Meta:
        widgets = {
            'caption': forms.TextInput()
        }
