from django.contrib import admin
from .models import Mall
from .models import MallImage
from .admin_forms import MallImageForm


class MallImageInline(admin.TabularInline):
    model = MallImage
    form = MallImageForm
    ordering = ('id',)
    extra = 0


class MallAdmin(admin.ModelAdmin):
    inlines = [MallImageInline, ]
    list_filter = ('weekends', )
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'slug', 'enquiry', 'email', 'address', 'open_time', 'closing_time', 'weekends')
    search_fields = ('name', 'slug', 'enquiry', 'email', 'address', 'open_time', 'closing_time', 'weekends')
    ordering = ('name',)
    list_display_links = ('name', 'slug')
    list_per_page = 50


admin.site.register(Mall, MallAdmin)

