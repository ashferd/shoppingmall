from django.shortcuts import render
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from .models import Mall

# Create your views here.


def index(request):
    malls = Mall.objects.all().order_by('id')
    paginator = Paginator(malls, 50)
    per_page = request.GET.get('per_page')
    if per_page:
        paginator = Paginator(malls, per_page)

    malls = paginator.get_page(1)
    page = request.GET.get('page')
    if page:
        malls = paginator.get_page(page)

    context = {'objects': malls}
    return render(request, 'malls/index.html', context)


def show(request, slug):
    mall = get_object_or_404(Mall, slug=slug)
    context = {'mall':mall}

    return render(request, 'malls/show.html', context)
