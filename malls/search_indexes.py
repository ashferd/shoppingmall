from haystack import indexes
from .models import Mall


class MallIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    slug = indexes.CharField(model_attr='slug')
    summary = indexes.CharField(model_attr='summary')
    address = indexes.CharField(model_attr='address')
    email = indexes.CharField(model_attr='email')
    enquiry = indexes.CharField(model_attr='enquiry')
    open_time = indexes.CharField(model_attr='open_time')
    closing_time = indexes.CharField(model_attr='closing_time')
    weekends = indexes.CharField(model_attr='weekends')
    note = indexes.CharField(model_attr='note')

    def get_model(self):
        return Mall

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
