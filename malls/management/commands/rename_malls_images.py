"""Rename malls image files."""

from django.conf import settings
from django.core.management.base import BaseCommand
from shoppingmall.services import rename_files
from shoppingmall.constants import MALL_IMAGE_PATH


class Command(BaseCommand):
    help = 'Rename malls image files.'

    def handle(self, *args, **options):
        rename_files(settings.MEDIA_ROOT + MALL_IMAGE_PATH)
