"""Generate 1k fake malls.py"""

import os
import random
from django.conf import settings
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify
from faker import Faker
from malls.models import Mall
from shoppingmall.services import ImageThumb
from shoppingmall.constants import MALL_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate 1k fake malls'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        mall_image_files = os.listdir(settings.MEDIA_ROOT + MALL_IMAGE_PATH)

        for index in range(10**3):
            name = fake.company()
            slug = "{}-{}".format(slugify(name), index)
            summary = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)
            address = fake.address()
            email = fake.company_email()
            enquiry = "+88-01682{}".format(fake.random_number(digits=6))
            open_time = fake.time(pattern="%H:%M:%S", end_datetime=None)
            closing_time = fake.time(pattern="%H:%M:%S", end_datetime=None)
            weekends = fake.day_of_week()
            note = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)
            image_name = random.choice(mall_image_files)
            cover_photo = '{}{}'.format(MALL_IMAGE_PATH, image_name)
            image_thumb_obj = ImageThumb()
            image_thumb_obj.generate_thumb(image_name, settings.MEDIA_ROOT + cover_photo, settings.MEDIA_ROOT + MALL_IMAGE_PATH)
            cover_photo_caption = "Photo from path: {}".format(cover_photo)

            try:
                new_mall = Mall.objects.create(
                    name=name,
                    slug=slug,
                    summary=summary,
                    address=address,
                    email=email,
                    enquiry=enquiry,
                    open_time=open_time,
                    closing_time=closing_time,
                    weekends=weekends,
                    note=note,
                    cover_photo=cover_photo,
                    cover_photo_caption=cover_photo_caption
                )
            except:
                pass