"""Generate 1k fake mall images"""

from os import listdir
import random
from django.core.management.base import BaseCommand
from django.conf import settings
from faker import Faker
from malls.models import Mall
from malls.models import MallImage
from shoppingmall.services import ImageThumb
from shoppingmall.constants import MALL_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate 1k fake mall images'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        mall_image_files = listdir(settings.MEDIA_ROOT + MALL_IMAGE_PATH)
        malls = Mall.objects.all()

        for index in range(10**3):
            mall = random.choice(malls)
            name = fake.name()
            image_name = random.choice(mall_image_files)
            image = '{}{}'.format(MALL_IMAGE_PATH, image_name)
            image_thumb_obj = ImageThumb()
            image_thumb_obj.generate_thumb(image_name, settings.MEDIA_ROOT+image, settings.MEDIA_ROOT+ MALL_IMAGE_PATH)
            caption = fake.sentence(nb_words=2, variable_nb_words=True, ext_word_list=None)

            try:
                new_mall_image = MallImage.objects.create(
                    mall=mall,
                    name=name,
                    image=image,
                    caption=caption
                )
            except:
                pass
