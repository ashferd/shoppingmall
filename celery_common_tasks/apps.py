from django.apps import AppConfig


class CeleryCommonTasksConfig(AppConfig):
    name = 'celery_common_tasks'
