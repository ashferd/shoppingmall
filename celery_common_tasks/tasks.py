from celery import shared_task


@shared_task(name='update_solr_index')
def update_solr_index():
    from haystack.management.commands import update_index
    update_index.Command().handle(using=['default'], remove=True)
