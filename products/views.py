from django.shortcuts import render
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from .models import Product

# Create your views here.


def index(request, category=None):
    if category:
        products = Product.objects.filter(category__slug=category).order_by('id')
    else:
        products = Product.objects.all().order_by('id')

    paginator = Paginator(products, 28)
    per_page = request.GET.get('per_page')
    if per_page:
        paginator = Paginator(products, per_page)

    products = paginator.get_page(1)
    page = request.GET.get('page')
    if page:
        products = paginator.get_page(page)

    context = {'objects': products}

    return render(request, 'products/index.html', context)


def show(request, slug):
    product = get_object_or_404(Product, slug=slug)
    context = {'product': product}

    return render(request, 'products/show.html', context)
