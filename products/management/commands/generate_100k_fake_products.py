"""Generate 100k fake products.py"""

import os
import random
from django.conf import settings
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify
from faker import Faker
from products.models import Product
from shops.models import Shop
from products.models import Category
from shoppingmall.services import ImageThumb
from shoppingmall.constants import PRODUCT_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate 100k fake products'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        product_image_files = os.listdir(settings.MEDIA_ROOT + PRODUCT_IMAGE_PATH)
        shops = Shop.objects.all()
        categories = Category.objects.filter(parent__isnull=False)

        for index in range(10**5):
            shop = random.choice(shops)
            category = random.choice(categories)
            name = fake.name()
            slugify(name)
            slug = "{}-{}".format(slugify(name), index)
            code = "SKU{}-00{}".format('P', index)
            specifications = fake.paragraph(nb_sentences=6, variable_nb_sentences=True, ext_word_list=None)
            price = (abs(9999-index))*index
            enquiry = "+88-01682{}".format(fake.random_number(digits=6))
            image_name = random.choice(product_image_files)
            cover_photo = '{}{}'.format(PRODUCT_IMAGE_PATH, image_name)
            image_thumb_obj = ImageThumb()
            image_thumb_obj.generate_thumb(image_name, settings.MEDIA_ROOT + cover_photo, settings.MEDIA_ROOT + PRODUCT_IMAGE_PATH)
            cover_photo_caption = "Photo from path: {}".format(cover_photo)
            image_name2 = random.choice(product_image_files)
            cover_photo2 = '{}{}'.format(PRODUCT_IMAGE_PATH, image_name2)
            cover_photo_caption2 = "Photo from path: {}".format(cover_photo2)
            note = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)

            try:
                new_product = Product.objects.create(
                    shop=shop,
                    category=category,
                    name=name,
                    slug=slug,
                    code=code,
                    specifications=specifications,
                    price=price,
                    enquiry=enquiry,
                    cover_photo=cover_photo,
                    cover_photo_caption=cover_photo_caption,
                    cover_photo2=cover_photo2,
                    cover_photo_caption2=cover_photo_caption2,
                    note=note
                )
            except:
                pass