""""Generate product category images"""

import random
from django.core.management.base import BaseCommand
from django.conf import settings
from google_images_download import google_images_download
from shoppingmall.constants import PRODUCT_CATEGORY_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate product category images'

    def handle(self, *args, **options):
        response = google_images_download.googleimagesdownload()
        items = ["jewelleries", "smart phones", "men's fashion", "women's fashion", "kid's fashion", "furniture",
                 "groceries", "foods & drinks"]

        exact_sizes = ['32,32', '64,64']

        for item in items:
            arguments = {
                "keywords": "{}".format(item),
                "format": "jpg",
                "exact_size": random.choice(exact_sizes),
                "type": 'photo',
                "limit": 100,
                "output_directory": settings.MEDIA_ROOT + PRODUCT_CATEGORY_IMAGE_PATH,
                "no_directory": True,
                "no_numbering": True,
                "print_urls": True
            }

            paths = response.download(arguments)
            print(paths)
