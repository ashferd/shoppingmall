"""Generate 50 fake subcategories.py"""

import os
import random
from django.conf import settings
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify
from faker import Faker
from products.models import Category
from shoppingmall.services import ImageThumb
from shoppingmall.constants import PRODUCT_CATEGORY_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate 50 fake subcategories'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        product_category_image_files = os.listdir(settings.MEDIA_ROOT + PRODUCT_CATEGORY_IMAGE_PATH)

        categories = Category.objects.filter(parent__isnull=True)
        for index in range(50):
            parent = random.choice(categories)
            name = fake.name()
            slug = "{}-{}".format(slugify(name), index)
            image_name = random.choice(product_category_image_files)
            photo = '{}{}'.format(PRODUCT_CATEGORY_IMAGE_PATH, image_name)
            image_thumb_obj = ImageThumb()
            image_thumb_obj.generate_thumb(image_name, settings.MEDIA_ROOT + photo, settings.MEDIA_ROOT + PRODUCT_CATEGORY_IMAGE_PATH)

            try:
                new_subcategory = Category.objects.create(
                    parent=parent,
                    name=name,
                    slug=slug,
                    photo=photo
                )
            except:
                pass
