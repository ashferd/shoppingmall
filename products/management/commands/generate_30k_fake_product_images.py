"""Generate 30k fake product images"""

from os import listdir
import random
from django.core.management.base import BaseCommand
from django.conf import settings
from faker import Faker
from products.models import Product
from products.models import ProductImage
from shoppingmall.services import ImageThumb
from shoppingmall.constants import PRODUCT_IMAGE_PATH


class Command(BaseCommand):
    help = 'Generate 30k fake product images'

    def handle(self, *args, **options):
        fake = Faker('en_US')
        product_image_files = listdir(settings.MEDIA_ROOT + PRODUCT_IMAGE_PATH)
        products = Product.objects.all()

        for index in range((10**4)*3):
            product = random.choice(products)
            name = fake.name()
            image_name = random.choice(product_image_files)
            image = '{}{}'.format(PRODUCT_IMAGE_PATH, image_name)
            image_thumb_obj = ImageThumb()
            image_thumb_obj.generate_thumb(image_name, settings.MEDIA_ROOT + image, settings.MEDIA_ROOT + PRODUCT_IMAGE_PATH)

            caption = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)

            try:
                new_product_image = ProductImage.objects.create(
                    product=product,
                    name=name,
                    image=image,
                    caption=caption
                )
            except:
                pass
