"""Rename product categories image files."""

from django.conf import settings
from django.core.management.base import BaseCommand
from shoppingmall.services import rename_files
from shoppingmall.constants import PRODUCT_CATEGORY_IMAGE_PATH


class Command(BaseCommand):
    help = 'Rename product categories image files.'

    def handle(self, *args, **options):
        rename_files(settings.MEDIA_ROOT + PRODUCT_CATEGORY_IMAGE_PATH)
