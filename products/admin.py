from django.contrib import admin
from .models import Category
from .models import Product
from .models import ProductImage
from .admin_forms import ProductImageForm


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('parent', 'name', 'slug')
    search_fields = ('name', 'slug')
    ordering = ('name',)
    list_display_links = ('name', 'slug')
    list_per_page = 20


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    form = ProductImageForm
    ordering = ('id',)
    extra = 0


class ProductImageAdmin(admin.ModelAdmin):
    list_display = ('product', 'name', 'image', 'caption')
    search_fields = ('product__name', 'name')
    ordering = ('name',)
    list_display_links = ('name', 'image')
    list_per_page = 100


class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductImageInline, ]
    list_filter = ('category', 'in_stock', )
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('shop', 'category', 'name', 'slug', 'code', 'price', 'in_stock', 'enquiry')
    search_fields = ('shop__name', 'category__name', 'name', 'slug', 'code', 'price', 'in_stock', 'enquiry')
    ordering = ('name',)
    list_display_links = ('name', 'slug')
    list_per_page = 100


admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductImage, ProductImageAdmin)
admin.site.register(Product, ProductAdmin)
