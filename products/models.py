from django.db import models
from django.urls import reverse
from slugger import AutoSlugField
from progressiveimagefield.fields import ProgressiveImageField
from phonenumber_field.modelfields import PhoneNumberField
from djmoney.models.fields import MoneyField
from shops.models import Shop
from shoppingmall.constants import PRODUCT_IMAGE_PATH
from shoppingmall.constants import PRODUCT_CATEGORY_IMAGE_PATH


class Category(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL, related_name='children')
    name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name')
    photo = ProgressiveImageField(upload_to=PRODUCT_CATEGORY_IMAGE_PATH, default='')

    class Meta:
        unique_together = ('slug', 'parent')
        verbose_name_plural = "categories"

    def get_absolute_url(self):
        return reverse('products:index', args=(self.slug,))

    def __str__(self):
        full_path = [self.name]

        k = self.parent

        while k is not None:
            full_path.append(k.name)
            k = k.parent

        return ' -> '.join(full_path[::-1])


class Product(models.Model):
    shop = models.ForeignKey(Shop, null=False, on_delete=models.CASCADE, related_name='products')
    category = models.ForeignKey(Category, null=False, on_delete=models.CASCADE, related_name='products')
    name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name', unique=True)
    code = models.CharField(max_length=250, unique=True,)
    specifications = models.TextField(blank=True, default='')
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='BDT')
    searchable_price = models.FloatField(null=False)
    in_stock = models.BooleanField(default=True)
    enquiry = PhoneNumberField(blank=False)
    cover_photo = ProgressiveImageField(upload_to=PRODUCT_IMAGE_PATH, default='')
    cover_photo_caption = models.TextField(blank=True, default='')
    cover_photo2 = ProgressiveImageField(upload_to=PRODUCT_IMAGE_PATH, default='')
    cover_photo_caption2 = models.TextField(blank=True, default='')
    note = models.TextField(blank=True, default='')

    def get_absolute_url(self):
        return reverse('products:show', args=(self.slug,))

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.searchable_price = self.price.amount
        super(Product, self).save(*args, **kwargs)


class ProductImage(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE, related_name='images')
    name = models.CharField(max_length=200)
    image = ProgressiveImageField(upload_to=PRODUCT_IMAGE_PATH, default='')
    caption = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name
