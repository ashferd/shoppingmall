from django import forms


class ProductImageForm(forms.ModelForm):
    class Meta:
        widgets = {
            'caption': forms.TextInput()
        }
