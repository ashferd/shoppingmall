from haystack import indexes
from .models import Product


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    slug = indexes.CharField(model_attr='slug')
    code = indexes.CharField(model_attr='code')
    specifications = indexes.CharField(model_attr='specifications')
    searchable_price = indexes.FloatField(model_attr='searchable_price', faceted=True)
    in_stock = indexes.BooleanField(model_attr='in_stock', faceted=True)
    enquiry = indexes.CharField(model_attr='enquiry')
    note = indexes.CharField(model_attr='note')

    def get_model(self):
        return Product

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
