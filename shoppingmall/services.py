import os
from os.path import split, splitext
from PIL import Image, ImageFilter
from django.db.models.fields.files import ImageField
from django.core.files.uploadedfile import InMemoryUploadedFile


def image_valid(path):
    try:
        image = Image.open(path)
        image.load()
        return True
    except OSError as e:
        return False
    except IOError as e:
        return False


def remove_invalid_image(full_path):
    filename, filexetension = splitext(full_path)

    if (
            (not os.path.isdir(full_path)) and (not filexetension)) \
            or (not image_valid(full_path)):

        os.remove(full_path)
        return True

    return False


THUMB_EXT = '_thumb'


def rename_files(path):
    f_list = os.listdir(path)

    m = 0
    for i in f_list:
        src = path + i
        if not remove_invalid_image(src):
            file_extension = os.path.splitext(i)[1]
            os.rename(src, path
                      + str(m) + file_extension)
            m = m + 1


class ImageThumb(ImageField):

    def in_memory(self, image):
        return type(image) is InMemoryUploadedFile

    def build_thumb_path(self, image_name, path_with_name, thumbnail_uploaded_path):
        """
        Build the absolute path of the to-be-saved thumbnail.
        """
        image_name_w_ext = split(image_name)[-1]
        image_name_generated, ext = splitext(image_name_w_ext)

        if not self.in_memory(path_with_name):
            # `image_file` is already in disk (not in memory).
            # `image_name` is the full path, not just the name
            image_name_generated = image_name_generated.split('/')[-1]

        return f'{thumbnail_uploaded_path}/{image_name_generated}{THUMB_EXT}{ext}'

    def generate_thumb(self, image_name, path_with_name, thumbnail_uploaded_path):
        """
        Given a (large) image, generate a 10x10px thumbnail
        with blur effect (in order to keep the size small)
        """
        try:
            picture = Image.open(path_with_name).convert('RGB')
            picture.thumbnail((10, 10))
            picture.filter(ImageFilter.GaussianBlur(radius=4))
            absolute_path = self.build_thumb_path(image_name, path_with_name, thumbnail_uploaded_path)
            self.save_thumb(picture, absolute_path)
        except:
            remove_invalid_image(path_with_name)

    def save_thumb(self, picture, absolute_path):
        picture.save(absolute_path)
