import os
from shoppingmall.settings import BASE_DIR

"""Global constants"""

"""Product image path inside media directory"""
PRODUCT_IMAGE_PATH = 'products/images/'

"""Product category image path inside media directory"""
PRODUCT_CATEGORY_IMAGE_PATH = 'product_categories/images/'

"""Shop image path inside media directory"""
SHOP_IMAGE_PATH = 'shops/images/'

"""Mall image path inside media directory"""
MALL_IMAGE_PATH = 'malls/images/'

"""'extra' shop image path inside extra directory. 'extra' shop directory will contain original data to copy in media"""
EXTRA_SHOP_IMAGE_PATH = os.path.join(BASE_DIR, 'extra/shops/images/')
