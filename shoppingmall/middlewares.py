from django.db import connection
from django.conf import settings


class SqlPrintMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if settings.DEBUG:
            response = self.get_response(request)
            time = 0
            print('No of queries - ', len(connection.queries))
            for query in connection.queries:
                time += float(query['time'])
            print('Time taken - ', time)
            print('Query - ', connection.queries)

            return response

        return None
