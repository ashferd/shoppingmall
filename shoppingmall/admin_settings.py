from django.contrib import admin


def configurations():
    admin.site.site_header = "Shopping mall administration"
    admin.site.site_title = "Shopping mall administration portal"
