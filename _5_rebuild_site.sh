#!/usr/bin/env bash

echo "======================"
echo "======================"
echo "Start faking data!"
echo "======================"
echo "======================"
echo "Creating main menu"
python manage.py generate_main_menu
echo "Main menu creation is done!"
echo "======================"
echo "Creating footer menu"
python manage.py generate_footer_menu
echo "Footer menu creation is done!"
echo "======================"
echo "Copy images from 'docs/demo_images/extra' directory to 'media' directory."
cp -Rf docs/demo_images/extra/** media/
echo "Copy images from 'extra' directory to 'media' directory is done!"
echo "======================"
echo "Creating Malls!"
python manage.py generate_1k_fake_malls
echo "Mall creation is done!"
echo "======================"
echo "Creating mall images entry."
python manage.py generate_1k_fake_mall_images
echo "Mall images creation is done!"
echo "======================"
echo "Creating shops!"
python manage.py generate_10k_fake_shops
echo "Shop creation is done!"
echo "======================"
echo "Creating shop images entry."
python manage.py generate_3k_fake_shop_images
echo "Shop images creation is done!"
echo "======================"
echo "Creating categories!"
python manage.py generate_10_fake_categories
echo "Category creation is done!"
echo "======================"
echo "Creating sub-categories!"
python manage.py generate_50_fake_subcategories
echo "Sub-category creation is done!"
echo "======================"
echo "Creating products!"
python manage.py generate_100k_fake_products
echo "Product creation is done!"
echo "======================"
echo "Creating product images entry."
python manage.py generate_30k_fake_product_images
echo "Product images creation is done!"
echo "======================"
echo "Rebuild solr index"
python manage.py rebuild_index
echo "Rebuild solr index is done!"
echo "======================"
echo "======================"
echo "Data is prepared! Done!!"
echo "========================"
echo "========================"